# React Todo Example

> React is a JavaScript library for creating user interfaces. Its core principles are declarative code, efficiency, and flexibility. Simply specify what your component looks like and React will keep it up-to-date when the underlying data changes.

> _[React - facebook.github.io/react](http://facebook.github.io/react)_

## Demo
View demo at Surge: [react-todo-example-adithya.surge.sh](http://react-todo-example-adithya.surge.sh)

## Learning React

The [React getting started documentation](http://facebook.github.io/react/docs/getting-started.html) is a great way to get started.

Here are some links you may find helpful:

* [Documentation](http://facebook.github.io/react/docs/getting-started.html)
* [API Reference](http://facebook.github.io/react/docs/reference.html)
* [Blog](http://facebook.github.io/react/blog/)
* [React on GitHub](https://github.com/facebook/react)
* [Support](http://facebook.github.io/react/support.html)
* [create-react-app Github Page](https://github.com/facebookincubator/create-react-app)

## Installing

Checkout the create-react-app Github Page for installation instructions.

Use the create-react-app to create a basic app, replace the **./public** and **./source** with the **./public** and **./source** from this example.

## Running

To run the app (after installing), locate the directory and issue the command, npm start (Refer create-react-app for more detailed usage)