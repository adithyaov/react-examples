import React, { Component } from 'react';
import './sakura/sakura-vader.css';


var itemCounter = 0;
var currentItemCounter = 0;
var checkedItemCounter = 0;

function HeaderComponent(props){
  return (
    <div>
      <h2>Todo React App</h2>
      <pre>
        A simple todo list using React JS.<br />
        Used for educational purpous only. :-)
      </pre>
    </div>
  )
}

function FooterComponent(props){
  return (
    <div>
      <pre>
        Click <span role="img" aria-label='check'>&#10063;</span> for checking/unchecking the item.<br />
        Click <span role="img" aria-label='remove'>&#10005;</span> for removing an item.<br />
        Click <span role="img" aria-label='edit'>&#9997;</span> for editing an item.<br />
      </pre>
    </div>
  )
}

function TodoListItem(props){
  return (
      <p>
        <button style={{'margin-right': 5}} onClick={props.DelFunc}><span role="img" aria-label='remove'>&#10005;</span></button>
        <button style={{'margin-right': 5}} onClick={props.StrikeFunc}><span role="img" aria-label='check'>&#10063;</span></button>
        <button style={{'margin-right': 15}} onClick={props.EditFunc}><span role="img" aria-label='edit'>&#9997;</span></button>
        <span style={props.checked === true ? {'text-decoration': 'line-through'} : {'text-decoration': 'none'}}>{props.content}</span>
      </p>
  )
}

class TodoListApp extends Component{
  constructor(props){
    super(props);
    this.state = {
      items: [],
      textInput: ''
    }
    this.addItem = this.addItem.bind(this)
    this.handleTextInput = this.handleTextInput.bind(this)
  }

  handleTextInput(e){
    e.preventDefault();
    this.setState({
      textInput: e.target.value
    })
  }

  strikeItem(itemId){
    var stateItems = this.state.items
    for(var i = 0; i < stateItems.length; i++){
      if(stateItems[i].id === itemId){
        stateItems[i].checked = !stateItems[i].checked;
        this.setState({
          items: stateItems
        })
        if(stateItems[i].checked === true){
          checkedItemCounter = checkedItemCounter + 1
        }else{
          checkedItemCounter = checkedItemCounter - 1
        }
        
        break;
      }
    }
  }

  editItem(itemId){
    var stateItems = this.state.items
    for(var i = 0; i < stateItems.length; i++){
      if(stateItems[i].id === itemId){
        var newVal = prompt("Please enter a new value", stateItems[i].content);
        if(newVal !== null){
          if(newVal.trim().length > 0){
            stateItems[i].content = newVal.trim()
            this.setState({
              items: stateItems
            })
          }
        }
        break;
      }
    }
  }

  delItem(itemId){
    var stateItems = this.state.items
    for(var i = 0; i < stateItems.length; i++){
      if(stateItems[i].id === itemId){
        if(stateItems[i].checked === true){
          checkedItemCounter = checkedItemCounter - 1
        }
        stateItems.splice(i, 1);
        this.setState({
          items: stateItems
        })
        currentItemCounter = currentItemCounter - 1
        break;
      }
    }
  }


  addItem(e){
    e.preventDefault();
    var stateItems = this.state.items
    if(this.state.textInput.trim().length > 0){
      stateItems.push({
          id: itemCounter,
          content: this.state.textInput.trim(),
          checked: false
      })
      this.setState({
        items: stateItems
      })
      currentItemCounter = currentItemCounter + 1
      itemCounter = itemCounter + 1;
    }
  }

  render(){
    var itemContent = ''
    var stateItems = this.state.items
    if(stateItems.length === 0){
      itemContent = <p>Add some items to your Todo List :-)</p>
    }else{
      itemContent = <ol>
        {
           stateItems.map((item) => (
             <li>
               <TodoListItem
               EditFunc={this.editItem.bind(this, item.id)}
               DelFunc={this.delItem.bind(this, item.id)}
               StrikeFunc={this.strikeItem.bind(this, item.id)}
               id={item.id} content={item.content} checked={item.checked}/></li>
             ))
        }
      </ol>
    }
    return (
      <div>
        <form onSubmit={this.addItem}>
          <input type='text' value={this.state.textInput} onChange={this.handleTextInput}/>
          <button type='submit' style={{'margin-left': 5}}>Add Item</button>
        </form>
        <pre>
          total number of items: {currentItemCounter}<br />
          number of items checked: {checkedItemCounter}
        </pre>
        <pre>
          {itemContent}
        </pre>
      </div>
    )
  }
}

class App extends Component{
  render(){
    return (
      <div>
        <HeaderComponent />
        <TodoListApp />
        <FooterComponent />
      </div>
    );
  }
}

export default App;
